variable "current_account_id" {
  description = "The account where this module is run."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.current_account_id == "" || can(regex("^([0-9]{12})?$", var.current_account_id))
    error_message = "“var.current_account_id” does not match '^[0-9]{12}$'."
  }
}

variable "current_region" {
  description = "The region where this module is run."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.current_region == "" || can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.current_region))
    error_message = "“var.current_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "current_partition" {
  description = "The current partition of the account."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.current_region == "" || can(regex("^aws.*$", var.current_partition))
    error_message = "“var.current_partition” does not match '^aws.*$'."
  }
}

variable "replica_region" {
  description = "The region where this module must replicate resources."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = var.replica_region == "" || var.replica_region != var.current_region
    error_message = "“var.replica_region” cannot be the same as “var.current_region”."
  }
  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.replica_region))
    error_message = "“var.replica_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "testing_prefix" {
  description = "Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.testing_prefix == "" || can(regex("^[a-z][a-zA-Z0-9]{3}$", var.testing_prefix))
    error_message = "“var.testing_prefix” does not match “^[a-z][a-zA-Z0-9]{3}$”."
  }
}

variable "tags" {
  description = "Tags to be shared among all resources of this module."
  default     = {}
  type        = map(string)
}

variable "replica_enabled" {
  description = "Whether to enable replica in another region for the Backup Vault."
  type        = bool
  default     = false
  nullable    = false
}
