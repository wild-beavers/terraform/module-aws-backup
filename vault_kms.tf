####
# KMS
####

variable "vault_kms_key" {
  description = <<-DOCUMENTATION
Options of the KMS Key to create.
  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - policy_json             (optional, string):      Additional policy to attach to the KMS key, merged with the resource-based policy.
  - description             (optional, string, ""):  Description of the key.
  - rotation_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the secrets.
  - deletion_window_in_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = optional(string, null)
    policy_json             = optional(string, null)
    description             = optional(string, "")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null

  validation {
    condition     = var.vault_kms_key == null || can(regex("[A-Za-z\\-_\\/]{1,256}", try(var.vault_kms_key.alias, "")))
    error_message = "“var.vault_kms_key.alias” is invalid."
  }
}

####
# Locals
####

locals {
  vault_kms_should_create = var.vault_kms_key != null

  vault_kms_key_id         = local.vault_kms_should_create ? module.kms["0"].aws_kms_keys["0"].arn : (var.vault.kms_key_arn != "" ? var.vault.kms_key_arn : null)
  vault_kms_replica_key_id = local.vault_kms_should_create && var.replica_enabled ? module.kms["0"].replica_aws_kms_keys["0"].arn : (var.vault.kms_key_replica_arn != "" ? var.vault.kms_key_replica_arn : null)
  vault_kms_iam_ro_actions = [
    "kms:Decrypt",
  ]
  vault_kms_iam_rw_actions = concat(
    [
      "kms:CreateGrant",
      "kms:GenerateDataKey",
      "kms:ListGrant",
      "kms:ListRetirableGrants",
      "kms:RetireGrant",
    ],
    local.vault_kms_iam_ro_actions
  )
  vault_kms_iam_delete_actions = [
    "kms:DisableKey",
    "kms:DisableKeyRotation",
    "kms:Delete*",
  ]
  vault_kms_iam_audit_actions = [
    "kms:DescribeCustomKeyStores",
    "kms:DescribeKey",
    "kms:GetKeyPolicy",
    "kms:GetKeyRotationStatus",
    "kms:GetParametersForImport",
  ]
  vault_kms_iam_actions = {
    ro     = local.vault_kms_iam_ro_actions
    rw     = local.vault_kms_iam_rw_actions
    rwd    = local.vault_kms_iam_rw_actions
    delete = local.vault_kms_iam_delete_actions
    audit  = local.vault_kms_iam_audit_actions
    full   = ["kms:*"]
  }
}

module "kms" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2.0"

  for_each = local.vault_kms_should_create ? { 0 = var.vault_kms_key } : {}

  prefix = var.testing_prefix

  kms_keys = {
    0 = {
      alias             = each.value.alias
      principal_actions = local.vault_kms_iam_actions
      service_principal = "backup"
      service_actions = concat(
        # https://docs.aws.amazon.com/aws-backup/latest/devguide/encryption.html
        [
          "kms:Encrypt",
          "kms:DescribeKey",
        ],
        local.vault_kms_iam_rw_actions
      )
      policy_json = each.value.policy_json

      description             = each.value.description
      rotation_enabled        = each.value.rotation_enabled
      deletion_window_in_days = each.value.deletion_window_in_days
      tags                    = each.value.tags
    }
  }

  replica_enabled = var.replica_enabled

  iam_policy_create                  = false
  iam_policy_export_json             = false
  iam_policy_entity_arns             = var.vault_iam_policy_entity_arns
  iam_policy_source_arns             = var.vault_iam_policy_source_arns
  iam_policy_sid_prefix              = var.vault_iam_policy_sid_prefix
  iam_policy_restrict_by_account_ids = var.vault_iam_policy_restrict_by_account_ids
  iam_policy_restrict_by_regions     = var.vault_iam_policy_restrict_by_regions

  tags = local.tags

  providers = {
    aws.replica = aws.replica
  }
}

data "aws_iam_policy_document" "identity_kms" {
  for_each = local.vault_should_generate_identity_based_policy_document ? local.vault_iam_all_used_scopes : []

  dynamic "statement" {
    for_each = local.vault_kms_should_create ? { 0 = 0 } : {}

    content {
      sid       = "${chomp(var.vault_iam_policy_sid_prefix)}KMS${upper(each.value)}"
      effect    = "Allow"
      actions   = local.vault_kms_iam_actions[each.value]
      resources = module.kms["0"].precomputed.aws_kms_aliases["0"].arns
    }
  }
}

####
# Outputs
####

output "aws_kms_key" {
  value = local.vault_kms_should_create ? module.kms["0"].aws_kms_keys["0"] : null
}

output "aws_kms_alias" {
  value = local.vault_kms_should_create ? module.kms["0"].aws_kms_aliases["0"] : null
}

output "replica_aws_kms_key" {
  value = local.vault_kms_should_create && var.replica_enabled ? module.kms["0"].replica_aws_kms_keys["0"] : null
}

output "replica_aws_kms_alias" {
  value = local.vault_kms_should_create && var.replica_enabled ? module.kms["0"].replica_aws_kms_aliases["0"] : null
}
