terraform {
  required_version = ">= 1.9"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 3.30"
      configuration_aliases = [aws.replica]
    }
  }
}
