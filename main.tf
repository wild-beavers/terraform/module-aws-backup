locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-backup"
    },
  )
}

####
# Plan
####

variable "plans" {
  description = <<-DESCRIPTION
One or more Backup Plans.
Keys are free values.
   - name                    (required(string)):          Name of the Backup Plan to create.
   - tags                    (optional(map(string), {})): Tags to assign specifically to the Backup Plan. Will be merged with `var.tags`.
   - windows_vss             (optional(bool, false)):     Enable Windows VSS backup option and create a VSS Windows backup.
   - rules                   (optional(map(object))):     Specifies a scheduled task that is used to back up a selection of resources. Keys are free values
      - name                         (required(string)): Display name for a backup rule.
      - schedule                     (optional(string)): CRON expression specifying when AWS Backup initiates a backup job.
      - schedule_expression_timezone (optional(string)): Timezone in which the schedule expression is set. Default value: `"Etc/UTC"`.
      - enable_continuous_backup     (optional(bool)):   Enable continuous backups for supported resources.
      - start_window                 (optional(number)): Amount of time in minutes before beginning a backup.
      - completion_window            (optional(number)): Amount of time in minutes AWS Backup attempts a backup before canceling the job and returning an error.
      - lifecycle                    (optional(object)): Lifecycle defines when a protected resource is transitioned to cold storage and when it expires.
         - cold_storage_after                        (optional(number)): Specifies the number of days after creation that a recovery point is moved to cold storage.
         - delete_after                              (optional(number)): Specifies the number of days after creation that a recovery point is deleted. Must be `90` days greater than `cold_storage_after`.
         - opt_in_to_archive_for_supported_resources (optional(bool)):   Setting will instruct your backup plan to transition supported resources to archive (cold) storage tier in accordance with your lifecycle settings.
      - recovery_point_tags          (optional(map(string))): Metadata that you can assign to help organize the resources that you create. Will be merged with `var.tags`.
      - copy_action                  (optional(string)):      Configuration block(s) with copy operation settings. When `var.replica` is `true` another `copy_action` to the replica vault will be created, regardless of this option.
        - destination_vault_arn (optional(string)): Amazon Resource Name (ARN) that uniquely identifies the destination backup vault for the copied backup.
        - lifecycle             (optional(object)): Lifecycle defines when a protected resource is transitioned to cold storage and when it expires.
           - cold_storage_after                        (optional(number)): Specifies the number of days after creation that a recovery point is moved to cold storage.
           - delete_after                              (optional(number)): Specifies the number of days after creation that a recovery point is deleted. Must be `90` days greater than `cold_storage_after`.
           - opt_in_to_archive_for_supported_resources (optional(bool)):   Setting will instruct your backup plan to transition supported resources to archive (cold) storage tier in accordance with your lifecycle settings.
DESCRIPTION
  type = map(object({
    name        = optional(string, "")
    tags        = optional(map(string), {})
    windows_vss = optional(bool, false)
    rules = optional(map(object({
      name                         = string
      schedule                     = optional(string)
      schedule_expression_timezone = optional(string)
      enable_continuous_backup     = optional(bool)
      start_window                 = optional(number)
      completion_window            = optional(number)
      lifecycle = optional(object({
        cold_storage_after                        = optional(number)
        delete_after                              = optional(number)
        opt_in_to_archive_for_supported_resources = optional(bool)
      }))
      recovery_point_tags = optional(map(string), {})
      copy_action = optional(object({
        destination_vault_arn = string
        lifecycle = optional(object({
          cold_storage_after                        = optional(number)
          delete_after                              = optional(number)
          opt_in_to_archive_for_supported_resources = optional(bool)
        }))
      }))
    })), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = alltrue([for plan in var.plans :
      can(regex("^[a-zA-Z0-9\\-\\_\\.]{2,64}$", plan.name))
    ])
    error_message = "One or more “var.plans.name” are invalid."
  }
  validation {
    condition = alltrue([for plan in var.plans :
      alltrue([for rule in plan.rules : (
        rule.schedule == null || can(regex("^cron\\([,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} .{1,3} .{1,3} [,*\\-0-9]{1,4}\\)$", rule.schedule))
      )])
    ])
    error_message = "One or more “var.plans.rules” are invalid."
  }
  validation {
    condition     = alltrue([for plan in var.plans : (length(plan.rules) > 0)])
    error_message = "One or more “var.plans” do not contain any rule."
  }
}

resource "aws_backup_plan" "this" {
  for_each = var.plans

  name = "${var.testing_prefix}${each.value.name}"
  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${each.value.name}"
    },
    each.value.tags,
  )

  dynamic "advanced_backup_setting" {
    for_each = each.value.windows_vss ? [1] : []

    content {
      backup_options = {
        WindowsVSS = "enabled"
      }
      resource_type = "EC2"
    }
  }

  dynamic "rule" {
    for_each = each.value.rules

    content {
      rule_name                = rule.value.name
      target_vault_name        = local.vault_current.name
      schedule                 = rule.value.schedule
      enable_continuous_backup = rule.value.enable_continuous_backup
      start_window             = rule.value.start_window
      completion_window        = rule.value.completion_window
      recovery_point_tags = merge(
        local.tags,
        rule.value.recovery_point_tags,
      )

      dynamic "lifecycle" {
        for_each = rule.value.lifecycle != null ? { 0 = rule.value.lifecycle } : {}

        content {
          cold_storage_after                        = lifecycle.value.cold_storage_after
          delete_after                              = lifecycle.value.delete_after
          opt_in_to_archive_for_supported_resources = lifecycle.value.opt_in_to_archive_for_supported_resources
        }
      }

      dynamic "copy_action" {
        for_each = lookup(rule.value, "copy_action", null) != null ? { 0 = lookup(rule.value, "copy_action", {}) } : {}

        content {
          destination_vault_arn = copy_action.value.destination_vault_arn

          dynamic "lifecycle" {
            for_each = rule.value.lifecycle != null ? { 0 = rule.value.lifecycle } : {}

            content {
              cold_storage_after                        = lifecycle.value.cold_storage_after
              delete_after                              = lifecycle.value.delete_after
              opt_in_to_archive_for_supported_resources = lifecycle.value.opt_in_to_archive_for_supported_resources
            }
          }
        }
      }

      dynamic "copy_action" {
        for_each = var.replica_enabled ? [1] : []

        content {
          destination_vault_arn = aws_backup_vault.replica["0"].arn

          dynamic "lifecycle" {
            for_each = rule.value.lifecycle != null ? { 0 = rule.value.lifecycle } : {}

            content {
              cold_storage_after                        = lifecycle.value.cold_storage_after
              delete_after                              = lifecycle.value.delete_after
              opt_in_to_archive_for_supported_resources = lifecycle.value.opt_in_to_archive_for_supported_resources
            }
          }
        }
      }
    }
  }
}

output "aws_backup_plans" {
  value = { for k, v in var.plans :
    k => { for i, j in aws_backup_plan.this[k] : i => j if !contains(["advanced_backup_setting", "tags", "version"], i) }
  }
}

####
# Selection
####

variable "selection_iam_role" {
  description = <<-DESCRIPTION
Role to be used for all `var.selections` missing a `iam_role_arn`.
   - name                  required(string):           Name of the IAM Role.
   - description           optional(string, ""):       Description of the IAM Role.
   - path                  optional(string, "/"):      Path of the IAM Role.
   - tags                  optional(map(string), {}):  Tags to assign to the role. Will be merge with `var.tags`.
   - policy_arns           optional(map(string)):      Policies granting the role access to backup and restore. Defaults to IAM service-roles policies provided by AWS. Keys are free values.
DESCRIPTION
  type = object({
    name        = optional(string, "")
    description = optional(string, "")
    path        = optional(string, "/")
    tags        = optional(map(string), {})
    policy_arns = optional(map(string), { 0 = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup" })
  })
  default = null

  validation {
    condition     = !local.selection_requires_role_creation || var.selection_iam_role != null
    error_message = "“var.selection_iam_role.name” is mandatory since one or more “var.selections” lacks a “iam_role_arn”."
  }
  validation {
    condition     = var.selection_iam_role == null || can(regex("^[a-zA-Z0-9+=,\\./@-]+$", try(var.selection_iam_role.name, "")))
    error_message = "“var.selection_iam_role.name” is invalid."
  }
  validation {
    condition     = can(regex("^\\/.*$", try(var.selection_iam_role.path, "/")))
    error_message = "“var.selection_iam_role.path” is invalid."
  }
  validation {
    condition = alltrue([for policy in try(var.selection_iam_role.policy_arns, {}) :
      try(provider::aws::arn_parse(policy).service, "no") == "iam" && startswith(try(provider::aws::arn_parse(policy).resource, "no"), "policy")
    ])
    error_message = "One or more “var.selection_iam_role.policy_arns” are invalid."
  }
}

variable "selections" {
  description = <<-DESCRIPTION
One or more Backup Selections.
Keys are free values.
   - name           (required(string)):       Display name of a resource selection document.
   - plan_reference (required(string)):       Key of `var.plans`, to associate this Backup Selection to.
   - iam_role_arn   (optional(string, "")):   ARN of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target resource. If omitted, this module will create a single IAM Role for all Backup Selections without `iam_role_arn`.
   - selection_tags (optional(list(object))): Tag-based conditions used to specify a set of resources to assign to a Backup Plan.
   - resources      (optional(list(string))): Strings that either contain ARNs or match patterns of resources to assign to a Backup Plan.
   - not_resources  (optional(list(sting))):  Strings that either contain ARNs or match patterns of resources to exclude to a Backup Plan.
DESCRIPTION
  type = map(object({
    name           = optional(string, "")
    plan_reference = optional(string, "")
    iam_role_arn   = optional(string, "")
    selection_tags = optional(list(object({
      type  = string
      key   = string
      value = string
    })))
    resources     = optional(list(string), [])
    not_resources = optional(list(string), [])
  }))
  default  = {}
  nullable = false

  validation {
    condition = alltrue([for selection in var.selections :
      can(regex("^[a-zA-Z0-9\\-\\_\\.]{2,64}$", selection.name))
    ])
    error_message = "One or more “var.selections.name” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.selections : (
      selection.iam_role_arn == "" || try(provider::aws::arn_parse(selection.iam_role_arn).service, "no") == "iam"
    )])
    error_message = "One or more “var.selections.iam_role_arn” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.selections : (
      contains(keys(var.plans), selection.plan_reference)
    )])
    error_message = "One or more “var.selections.plan_reference” does not exist."
  }
  validation {
    condition = alltrue([for selection in var.selections : (
      selection.selection_tags != null || length(selection.resources) > 0 || length(selection.not_resources) > 0
    )])
    error_message = "One or more “var.selections” misses either a “selection_tags” or “resources”."
  }
  validation {
    condition = alltrue([for selection in var.selections : (
      alltrue([for arn in selection.resources : (
        try(provider::aws::arn_parse(arn).service, "no") != "no"
      )])
    )])
    error_message = "One or more “var.selections.resources” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.selections : (
      alltrue([for arn in selection.not_resources : (
        try(provider::aws::arn_parse(arn).service, "no") != "no"
      )])
    )])
    error_message = "One or more “var.selections.not_resources” are invalid."
  }
}

locals {
  selection_requires_role_creation = !alltrue([for selection in var.selections : selection.iam_role_arn != ""])
}

data "aws_iam_policy_document" "selection_assume_role" {
  for_each = var.selection_iam_role != null || var.restore_selection_iam_role != null ? { 0 = 0 } : {}

  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = ["backup.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "backup_selection" {
  for_each = var.selection_iam_role != null ? { 0 = 0 } : {}

  name               = "${var.testing_prefix}${var.selection_iam_role.name}"
  description        = var.selection_iam_role.description
  path               = var.selection_iam_role.path
  assume_role_policy = data.aws_iam_policy_document.selection_assume_role["0"].json

  tags = merge(
    local.tags,
    {
      Name        = "${var.testing_prefix}${var.selection_iam_role.name}"
      Description = var.selection_iam_role.description
    },
    var.selection_iam_role.tags,
  )
}

resource "aws_iam_role_policy_attachment" "backup_selection" {
  for_each = var.selection_iam_role != null ? var.selection_iam_role.policy_arns : {}

  policy_arn = each.value
  role       = aws_iam_role.backup_selection["0"].name
}

resource "aws_iam_role_policy_attachment" "backup_selection_additional" {
  for_each = local.should_create_selection_additional_iam_policy && var.selection_iam_role != null ? { 0 = 0 } : {}

  policy_arn = aws_iam_policy.selection_additional_iam_policy["0"].arn
  role       = aws_iam_role.backup_selection["0"].name
}

resource "aws_backup_selection" "this" {
  for_each = var.selections

  iam_role_arn = each.value.iam_role_arn != "" ? each.value.iam_role_arn : aws_iam_role.backup_selection["0"].arn
  name         = each.value.name
  plan_id      = aws_backup_plan.this[each.value.plan_reference].id

  dynamic "selection_tag" {
    for_each = each.value.selection_tags != null ? each.value.selection_tags : []

    content {
      type  = selection_tag.value.type
      key   = selection_tag.value.key
      value = selection_tag.value.value
    }
  }

  resources     = length(each.value.resources) > 0 ? each.value.resources : null
  not_resources = length(each.value.not_resources) > 0 ? each.value.not_resources : null

  depends_on = [
    aws_iam_role_policy_attachment.backup_selection,
  ]
}

output "backup_selection_aws_iam_role" {
  value = var.selection_iam_role != null ? { for k, v in aws_iam_role.backup_selection["0"] :
    k => v if !contains(["assume_role_policy", "name_prefix", "tags", "inline_policy", "managed_policy_arns", "permissions_boundary"], k)
  } : null
}

####
# IAM Policy
####

variable "selection_additional_iam_policy" {
  description = <<-DESCRIPTION
IAM Policy to attach to the `var.selection_iam_role` and `var.restore_selection_iam_role`, in additions to these variables own `policy_arns`.
The policy is intended to restrict access to specific resources.

   - name                  required(string):           Name of the IAM Role.
   - description           optional(string, ""):       Description of the IAM Role.
   - path                  optional(string, "/"):      Path of the IAM Role.
   - tags                  optional(map(string), {}):  Tags to assign to the role. Will be merge with `var.tags`.
   - json_policy           optional(string)):          JSON Policy to attach to the additional IAM Policy.
   - restricted_tags       optional(map(string)):      Add statements to the policy for every tag:value given. When resources are tagged, grants the policy access to the given combinations only.
   - grant_kms_access      optional(bool, true):       Add a statement to grant the policy access to the vault KMS key ID (along with the replica vault KMS key ID, should it be enabled).
DESCRIPTION
  type = object({
    name             = optional(string, "")
    description      = optional(string, "")
    path             = optional(string, "/")
    tags             = optional(map(string), {})
    json_policy      = optional(string, "")
    restricted_tags  = optional(map(string), {})
    grant_kms_access = optional(bool, true)
  })
  default = null

  validation {
    condition     = var.selection_additional_iam_policy == null || can(regex("^[a-zA-Z0-9+=,\\./@-]+$", try(var.selection_additional_iam_policy.name, "")))
    error_message = "“var.selection_additional_iam_policy.name” is invalid."
  }
  validation {
    condition     = (local.selection_requires_role_creation || local.restore_selection_requires_role_creation) || try(var.selection_additional_iam_policy.name, "") == ""
    error_message = "“var.selection_additional_iam_policy.name” is set, but no role should be created within this module."
  }
  validation {
    condition     = can(regex("^\\/.*$", try(var.selection_additional_iam_policy.path, "/")))
    error_message = "“var.selection_additional_iam_policy.path” is invalid."
  }
  validation {
    condition     = try(var.selection_additional_iam_policy.json_policy, "") == "" || can(jsondecode(try(var.selection_additional_iam_policy.json_policy, "")))
    error_message = "“var.selection_additional_iam_policy.json_policy” is invalid."
  }
}

locals {
  should_create_selection_additional_iam_policy = var.selection_additional_iam_policy != null
}

resource "aws_iam_policy" "selection_additional_iam_policy" {
  for_each = local.should_create_selection_additional_iam_policy ? { 0 = 0 } : {}

  name        = "${var.testing_prefix}${var.selection_additional_iam_policy.name}"
  description = var.selection_additional_iam_policy.description
  path        = var.selection_additional_iam_policy.path
  tags = merge(
    local.tags,
    {
      Name        = "${var.testing_prefix}${var.selection_additional_iam_policy.name}"
      Description = var.selection_additional_iam_policy.description
    },
    var.selection_additional_iam_policy.tags,
  )
  policy = local.should_create_selection_additional_iam_policy ? data.aws_iam_policy_document.selection_additional_iam_policy["0"].json : ""
}

data "aws_iam_policy_document" "selection_additional_iam_policy" {
  for_each = local.should_create_selection_additional_iam_policy ? { 0 = 0 } : {}

  source_policy_documents = var.selection_additional_iam_policy.json_policy != "" ? [var.selection_additional_iam_policy.json_policy] : []

  statement {
    effect  = "Allow"
    actions = local.vault_iam_rw_actions
    resources = concat(
      [local.vault_arn],
      var.replica_enabled ? [local.vault_replica_arn] : [],
    )
  }

  dynamic "statement" {
    for_each = var.selection_additional_iam_policy.restricted_tags

    content {
      effect    = "Deny"
      actions   = ["*"]
      resources = ["*"]
      condition {
        test     = "StringNotEquals"
        values   = [statement.value]
        variable = "aws:ResourceTag/${statement.key}"
      }
      condition {
        test     = "Null"
        values   = ["false"]
        variable = "aws:ResourceTag/${statement.key}"
      }
    }
  }
}

output "selection_additional_aws_iam_policy" {
  value = local.should_create_selection_additional_iam_policy ? { for k, v in aws_iam_policy.selection_additional_iam_policy["0"] :
    k => v if !contains(["policy", "name_prefix", "tags", "attachment_count", "permissions_boundary"], k)
  } : null
}
