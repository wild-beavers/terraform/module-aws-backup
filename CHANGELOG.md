## 4.2.0

- feat: adds `delete` for delete-only scope

## 4.1.3

- chore: nothing new, just for pipeline re run.

## 4.1.2

- fix: Terraform known-before-apply complaining
- fix: additional "ro" and "rw" scoped entity ARNS were ignored when internal roles were enabled
- fix: adds default service-role for restore in internal policy, when resource-based policy is needed and `var.restore_testing_plans` is set

## 4.1.1

- fix: resource-based policy was not granting sufficient permission for restore.

## 4.1.0

- feat: adds `var.selection_additional_iam_policy` to create an additional policies for plans roles
- feat: the module’s `var.selection_additional_iam_policy` can now optionally grant access to the KMS keys
- fix: role assigned to restore selection was incorrect, when using module iam role
- fix: policy for replica vault was not applied
- fix: copy actions were not working correctly, due to lack of KMS resource-based access from the vaults.

## 4.0.0

- feat: (BREAKING) requires Terraform 1.9+
- feat: (BREAKING) requires `aws.replica` AWS terraform provider
- feat: (BREAKING) variables replacements:
   - `region_settings_resource_type_opt_in` => `region_settings.region_settings_resource_type_opt_in`
   - `vault_enabled` => *removed* (use `vault_kms_key.alias`)
   - `vault_policy_enabled` => *removed* (use `vault.policy`)
   - `vault_lock_enabled` => *removed* (use `vault.lock_…`)
   - `vault_kms_key_enabled` => *removed* (use `vault_kms_key.alias`)
   - `vault_kms_replica_key_enabled` => *removed* (use `replica_enabled`)
   - `vault_kms_key_multi_region` => *removed* (use `replica_enabled`)
   - `vault_name` => `vault.name`
   - `vault_policy` => `vault.policy`
   - `vault_force_destroy` => `vault.force_destroy`
   - `vault_lock_configuration_changeable_for_days` => `vault.lock_changeable_for_days`
   - `vault_lock_configuration_max_retention_days` => `vault.lock_max_retention_days`
   - `vault_lock_configuration_min_retention_days` => `vault.lock_min_retention_days`
   - `vault_lock_configuration_min_retention_days` => `vault.lock_min_retention_days`
   - `vault_kms_key_arn` => `vault.kms_key_arn`
   - `vault_tags` => `vault.tags`
   - *none* => `vault.kms_key_replica_arn`
   - `vault_kms_key_name` => `vault.kms_key_arn`
   - `vault_kms_key_policy` => `vault_kms_key.policy_json`
   - `vault_kms_key_enable_key_rotation` => `vault_kms_key.rotation_enabled`
   - `vault_kms_key_alias_name` => `vault_kms_key.alias`
   - `vault_kms_tags` => `vault_kms_key.tags`
- feat: (BREAKING) changes outputs:
   - `vault_id` => `aws_backup_vault.id`
   - `vault_arn` => `aws_backup_vault.arn`
   - `vault_recovery_points` => *removed*
   - `vault_kms_key_arn` => `aws_kms_key.arn`
   - `vault_kms_key_id` => `aws_kms_key.id`
   - `vault_kms_key_alias_arn` => `aws_kms_alias.arn`
   - `vault_kms_key_replica_arn` => `replica_aws_kms_key.arn`
   - `vault_kms_key_replica_id` => `replica_aws_kms_key.id`
   - `selection_tag_id` => *removed*
   - `selection_resources_ids` => *removed*
   - `selection_iam_role_arn` => `backup_selection_aws_iam_role.arn`
   - `selection_iam_role_name` => `backup_selection_aws_iam_role.name`
   - `selection_iam_role_unique_id` => `backup_selection_aws_iam_role.unique_id`
   - `plan_arns` => `aws_backup_plan.xxx.arn`
   - `plan_versions` => *removed*
- feat: supports replica vault internally, with optional replica KMS
- feat: supports resource-based policy for the vault
- feat: supports notifications through SNS for the vault
- feat: supports backup restore testing plans & selections
- refactor: changes internal module configuration, using KMS and IAM Policy modules
- chore: bump pre-commit hooks

## 3.0.1

- chore: bump pre-commit hooks
- doc: updates CHANGELOG to latest practices
- fix: role didn’t have any tags
- test: refactor examples to fix various issues

## 3.0.0

- maintenance: (BREAKING) bump the minimal Terraform version to ## 1.3+
- chore: bump pre-commit hooks
- test: remove Jenkinsfile

## 2.2.0

- feat: adds `var.vault_force_destroy` to control deletion of recovery points

## 2.1.1

- maintenance: fix TF lint issues
- chore: bump pre-commit hooks
- tests: adds gitlab ci

## 2.1.0

- feat: allows to create a KMS key replica, for multi-region
- feat: adds lifecycle to "copy_action"
- fix: adds a required provider: aws
- test: sets test EBS size to 1GB

## 2.0.0

- feat: allow to specify multiple tags for selection by tags
- feat: allow to use external policies to attach to the Selection Backup role
- feat: allow to lock the vault recovery points for a period of time, preventing any kind of deletion.
- feat: allow to add a policy to the AWS Backup vault.
- feat: add `var.global_settings_cross_account_enabled` setting to allow cross account backups
- feat: add `var.region_settings_resource_type_opt_in` setting to opt-in/opt-out to some services for backup
- refactor: (BREAKING) modernize practices: renames, for_eaches, resource validations, descriptions…
- refactor: (BREAKING) Automatically prefix KMS alias with `alias/`
- refactor: removes descriptions from outputs
- tech: removes AUTHORS file
- tech: empty Jenkinsfile
- tech: update .gitignore
- test: cleans examples
- test: merges external examples together
- maintenance: updates .pre-commit dependencies
- tech: (BREAKING) terraform 1+ upgrade
- tech: add LICENSE

## 1.0.2

- chore: bump pre-commit hooks
- fix: Add `AWSBackupServiceRolePolicyForRestores` to IAM role in order to backup KMS protected dynamodb ressources

## 1.0.1

- fix: Change pre-commit version of hooks and apply
- refactor: Remove quotes on keywords

## 1.0.0

- breaking: terraform ## 0.12 upgrade & best practices

## 0.1.0

- Initial commit
