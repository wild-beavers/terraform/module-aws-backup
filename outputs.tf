output "precomputed" {
  value = merge(
    var.vault != null ? {
      aws_backup_vault_arn = local.vault_arn
    } : {},
    var.vault != null && !var.vault.logically_air_gapped && var.replica_enabled ? {
      aws_backup_vault_replica_arn = local.vault_replica_arn
    } : {},
    var.vault_kms_key != null ? {
      vault_kms_key = module.kms["0"].precomputed
    } : {},
    var.vault_iam_policy_identity != null ? {
      aws_iam_policies = module.iam_policy["0"].precomputed.aws_iam_policies
    } : {},
    var.vault_iam_policy_export_actions ? {
      actions_aws_iam_policies = { for scope, scope_actions in local.vault_iam_scoped_actions :
        scope => lookup(local.vault_iam_identity_scoped_actions_overrides, scope, local.vault_iam_scoped_actions[scope])
      }
      kms_actions_aws_iam_policies = local.vault_kms_iam_actions
    } : {},
    var.selection_iam_role != null ? {
      backup_selection_aws_iam_role_arn = provider::aws::arn_build(local.current_partition, "iam", "", local.current_account_id, "role${var.selection_iam_role.path}${var.testing_prefix}${var.selection_iam_role.name}")
    } : {},
    var.restore_selection_iam_role != null ? {
      restore_selection_aws_iam_role_arn = provider::aws::arn_build(local.current_partition, "iam", "", local.current_account_id, "role${var.restore_selection_iam_role.path}${var.testing_prefix}${var.restore_selection_iam_role.name}")
    } : {},
  )
}

locals {
  vault_arn         = provider::aws::arn_build(local.current_partition, "backup", local.current_region, local.current_account_id, "backup-vault:${var.testing_prefix}${var.vault.name}")
  vault_replica_arn = provider::aws::arn_build(local.current_partition, "backup", local.replica_region, local.current_account_id, "backup-vault:${var.testing_prefix}${var.vault.name}")
}
