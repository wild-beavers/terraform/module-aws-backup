####
# Variable
####

variable "vault_iam_policy_identity" {
  description = <<-DOCUMENTATION
Identity-based IAM Policies to create.
Only policies fitting resource-based user-provided scopes (`ro`, `rw`, `rwd`, `delete`, `audit` and `full`) will be created.
   - name_template           (required, string):           Name template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `delete`, `audit` and `full`).
   - description_template    (required, string):           Description template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `delete`,  `audit` and `full`).
   - path                    (optional, string, "/"):      Path of the IAM Policies.
   - tags                    (optional, map(string), {}):  Tags of the IAM Policies. Will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name_template        = string
    description_template = string
    path                 = optional(string, "/")
    tags                 = optional(map(string), {})
  })
  default = null
}

variable "vault_iam_policy_identity_document" {
  description = <<-DOCUMENTATION
Arguments specific for the identity-based Policy documents.
Mandatory if `var.iam_policy_identity`, but might be used solely in combination with `var.iam_policy_identity_export_jsons`.
   - source_policy_documents (optional, list(string), []): List of JSON strings to be concatenated to the identity-based polices.
   - resources_arns          (optional(list(string), []):  ARNs of resources created outside this module to compose the IAM identity-based Policies with.
DOCUMENTATION
  type = object({
    source_policy_documents = optional(list(string), [])
    resources_arns          = optional(list(string), [])
  })
  default  = {}
  nullable = false
}

variable "vault_iam_policy_identity_export_jsons" {
  description = "Whether to export identity-based IAM policies contents as JSON. Setting this to `true` do not create any policies. This can be used instead of or in combination with `var.external_iam_policy`."
  type        = bool
  default     = false
  nullable    = false
}

variable "vault_iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "vault_iam_policy_export_actions" {
  description = "Whether or not to export IAM policies actions. A lightweight way to generate your own policies."
  type        = bool
  default     = false
  nullable    = false
}

variable "vault_iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users) for the resource-based policy. Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `delete`, `audit` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  default     = {}
  nullable    = false
}

variable "vault_iam_policy_source_arns" {
  description = "Restrict accesses to the given source ARNs for the resource-based policy. Wildcards are allowed. Keys are scope: `ro`, `rw`, `rwd`, `delete`, `audit` or `full`, each specifying the expected level of access for entities defined within."
  type        = map(map(string))
  default     = {}
  nullable    = false
}

variable "vault_iam_policy_restrict_by_regions" {
  description = "Restrict resource-based IAM Policy by the given regions. Resource-Based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "vault_iam_policy_restrict_by_account_ids" {
  description = "Restrict IAM Policy by the given account IDs. Resource-based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

####
# Resources
####

locals {
  vault_should_generate_identity_based_policy_document = var.vault_iam_policy_identity != null || var.vault_iam_policy_identity_document != null || var.vault_iam_policy_identity_export_jsons
  vault_should_have_resource_based_policy              = var.vault.policy != "" || length(var.vault_iam_policy_entity_arns) > 0 || length(var.vault_iam_policy_source_arns) > 0

  vault_iam_ro_actions = [
    "backup:CopyFromBackupVault",
    "backup:DescribeBackupVault",
    "backup:DescribeRecoveryPoint",
    "backup:GetRecoveryPointRestoreMetadata",
    "backup:ListProtectedResourcesByBackupVault",
    "backup:ListRecoveryPointsByBackupVault",
    "backup:StartCopyJob",
    "backup:StartRestoreJob",
  ]
  vault_iam_rw_actions = concat(
    local.vault_iam_ro_actions,
    [
      "backup:CopyIntoBackupVault",
      "backup:DisassociateRecoveryPoint",
      "backup:DisassociateRecoveryPointFromParent",
      "backup:UpdateRecoveryPointLifecycle",
      "backup:StartBackupJob",
    ]
  )
  vault_iam_rwd_actions = concat(
    local.vault_iam_rw_actions,
    [
      "backup:DeleteRecoveryPoint",
    ]
  )
  vault_iam_audit_actions = [
    "backup:DescribeBackupVault",
    "backup:DescribeRecoveryPoint",
    "backup:GetBackupVaultAccessPolicy",
    "backup:GetBackupVaultNotifications",
    "backup:GetRecoveryPointRestoreMetadata",
    "backup:ListProtectedResourcesByBackupVault",
    "backup:ListRecoveryPointsByBackupVault",
  ]
  vault_iam_delete_actions = [
    # Due to AWS limitation, resource-based policy for backup cannot allow complete deletion
    # See "identity_scoped_actions_overrides" for corresponding identity-based policy actions
    "backup:DeleteRecoveryPoint",
    "backup:DeleteBackupVaultLockConfiguration",
    "backup:DeleteBackupVaultNotifications",
    "backup:DisassociateRecoveryPoint",
    "backup:DisassociateRecoveryPointFromParent",
  ]
  vault_iam_full_actions = concat(
    local.vault_iam_rwd_actions,
    [
      "backup:DeleteBackupVaultLockConfiguration",
      "backup:DeleteBackupVaultNotifications",
      "backup:GetBackupVaultAccessPolicy",
      "backup:GetBackupVaultNotifications",
      "backup:PutBackupVaultAccessPolicy",
      "backup:PutBackupVaultLockConfiguration",
      "backup:PutBackupVaultNotifications",
    ]
  )
  vault_iam_scoped_actions = {
    ro     = local.vault_iam_ro_actions
    rw     = local.vault_iam_rw_actions
    rwd    = local.vault_iam_rwd_actions
    delete = local.vault_iam_delete_actions
    audit  = local.vault_iam_audit_actions
    full   = local.vault_iam_full_actions
  }
  vault_iam_identity_scoped_actions_overrides = {
    delete = [
      "backup:Delete*",
      "backup:Disassociate*",
    ]
  }

  vault_iam_all_used_scopes = toset(distinct(concat(keys(var.vault_iam_policy_entity_arns), keys(var.vault_iam_policy_source_arns))))
  vault_identity_iam_policy_document = local.vault_should_generate_identity_based_policy_document ? {
    source_policy_documents = var.vault_iam_policy_identity_document.source_policy_documents
    scoped_source_policy_documents = { for scope, doc in data.aws_iam_policy_document.identity_kms :
      scope => [doc.json]
    }
    resources_arns = concat(
      try(var.vault_iam_policy_identity_document.resources_arns, []),
      var.vault != null ? [
        local.vault_arn
      ] : [],
      var.replica_enabled ? [
        local.vault_replica_arn
      ] : []
    )
  } : null
}

module "iam_policy" {
  source  = "gitlab.com/wild-beavers/module-aws-iam-policy/aws"
  version = "~> 1.0"

  for_each = local.vault_should_have_resource_based_policy || local.vault_should_generate_identity_based_policy_document ? { 0 = "enabled" } : {}

  current_account_id = local.current_account_id
  testing_prefix     = var.testing_prefix
  tags               = var.tags

  entity_arns = local.vault_should_have_resource_based_policy ? merge(
    var.restore_selection_iam_role != null || length(lookup(var.vault_iam_policy_entity_arns, "ro", {})) > 0 || length(var.restore_testing_plans) > 0 ? {
      ro = merge(
        var.restore_selection_iam_role != null ? {
          restore_selection = aws_iam_role.restore_selection["0"].arn
        } : {},
        length(var.restore_testing_plans) > 0 ? {
          service = "arn:aws:iam::${local.current_account_id}:role/aws-service-role/restore-testing.backup.amazonaws.com/AWSServiceRoleForBackupRestoreTesting"
        } : {},
        lookup(var.vault_iam_policy_entity_arns, "ro", {}),
      )
    } : {},
    var.selection_iam_role != null || length(lookup(var.vault_iam_policy_entity_arns, "rw", {})) > 0 ? {
      rw = merge(
        var.selection_iam_role != null ? {
          backup_selection = aws_iam_role.backup_selection["0"].arn
        } : {},
        lookup(var.vault_iam_policy_entity_arns, "rw", {}),
      )
    } : {},
    length(lookup(var.vault_iam_policy_entity_arns, "rwd", {})) > 0 ? {
      rwd = var.vault_iam_policy_entity_arns.rwd
    } : {},
    length(lookup(var.vault_iam_policy_entity_arns, "delete", {})) > 0 ? {
      delete = var.vault_iam_policy_entity_arns.delete
    } : {},
    length(lookup(var.vault_iam_policy_entity_arns, "audit", {})) > 0 ? {
      audit = var.vault_iam_policy_entity_arns.audit
    } : {},
    length(lookup(var.vault_iam_policy_entity_arns, "full", {})) > 0 ? {
      full = var.vault_iam_policy_entity_arns.full
    } : {},
  ) : null
  resource_arns                     = ["*"]
  scoped_actions                    = local.vault_iam_scoped_actions
  sid_prefix                        = var.vault_iam_policy_sid_prefix
  restrict_by_regions               = var.vault_iam_policy_restrict_by_regions
  restrict_by_account_ids           = var.vault_iam_policy_restrict_by_account_ids
  source_policy_documents           = var.vault.policy != "" ? [var.vault.policy] : null
  identity_iam_policy               = var.vault_iam_policy_identity
  identity_iam_policy_document      = local.vault_identity_iam_policy_document
  identity_export_jsons             = var.vault_iam_policy_identity_export_jsons
  identity_scoped_actions_overrides = local.vault_iam_identity_scoped_actions_overrides
}

####
# Outputs
####

output "aws_iam_policies" {
  value = local.vault_should_have_resource_based_policy || local.vault_should_generate_identity_based_policy_document ? module.iam_policy["0"].aws_iam_policies : null
}
