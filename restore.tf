####
# Variables
####

variable "restore_testing_plans" {
  description = <<-DESCRIPTION
One or more restore testing Backup Plans.
Keys are free values.
   - name                         (required(string)): Name of the restore testing Plan.
   - schedule_expression          (required(string)): Schedule expression for the restore testing Plan.
   - schedule_expression_timezone (optional(string)): Timezone for the schedule expression. If not provided, the state value will be used.
   - start_window_hours           (optional(number)): Number of hours in the start window for the restore testing plan.
   - recovery_point_selection     (required(object):  Specifies the recovery point selection configuration.
      - algorithm             (required(string)):       Algorithm used for selecting recovery points. Valid values are "RANDOM_WITHIN_WINDOW" and "LATEST_WITHIN_WINDOW".
      - recovery_point_types  (required(list(string))): Types of recovery points to include in the selection. Valid values are "CONTINUOUS" and "SNAPSHOT".
      - selection_window_days (optional(number)):       Number of days within which the recovery points should be selected. Must be a value between 1 and 365.
DESCRIPTION
  type = map(object({
    name                         = optional(string, "")
    schedule_expression          = optional(string, "")
    schedule_expression_timezone = optional(string)
    start_window_hours           = optional(number)
    recovery_point_selection = optional(object({
      algorithm             = optional(string)
      recovery_point_types  = optional(list(string))
      selection_window_days = optional(number)
    }), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = alltrue([for plan in var.restore_testing_plans :
      can(regex("^[a-zA-Z0-9\\_]{1,50}$", plan.name))
    ])
    error_message = "One or more “var.restore_testing_plans.name” are invalid."
  }
  validation {
    condition = alltrue([for plan in var.restore_testing_plans :
      can(regex("^cron\\([,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} .{1,3} .{1,3} [,*\\-0-9]{1,4}\\)$", plan.schedule_expression))
    ])
    error_message = "One or more “var.restore_testing_plans.schedule_expression” are invalid."
  }
  validation {
    condition = alltrue([for plan in var.restore_testing_plans : (
      plan.start_window_hours == null || (plan.start_window_hours > 0 && plan.start_window_hours <= 168)
    )])
    error_message = "One or more “var.restore_testing_plans.start_window_hours” are invalid."
  }
  validation {
    condition = alltrue([for plan in var.restore_testing_plans : (
      plan.name == "" || (
        length(plan.recovery_point_selection) != 0 &&
        contains(["RANDOM_WITHIN_WINDOW", "LATEST_WITHIN_WINDOW"], plan.recovery_point_selection.algorithm) &&
        length(setsubtract(plan.recovery_point_selection.recovery_point_types, ["CONTINUOUS", "SNAPSHOT"])) == 0 &&
        coalesce(lookup(plan.recovery_point_selection, "selection_window_days", 1), 1) > 0 && coalesce(lookup(plan.recovery_point_selection, "selection_window_days", 1), 1) <= 365
    ))])
    error_message = "One or more “var.restore_testing_plans.recovery_point_selection” are invalid."
  }
}

variable "restore_testing_selections" {
  description = <<-DESCRIPTION
One or more Backup Selections for restore testing.
Keys are free values.
   - name                          (required(string)):       Display name of a resource selection document for restore testing.
   - testing_plan_reference        (required(string)):       Key of `var.restore_testing_plans`, to associate this restore testing Backup Selection to.
   - iam_role_arn                  (optional(string, "")):   ARN of the IAM role that AWS Backup uses to authenticate when restoring the target resources. If omitted, this module will create a single IAM Role for all restore testing Backup Selections without `iam_role_arn`.
   - protected_resource_conditions (optional(list(object))): Tag-based conditions used to specify a set of resources to assign to a restore testing Backup Plan.
      - string_equals      (optional(list(object))): List of conditions for resource tags. Filters the values of your tagged resources for only those resources that you tagged with the same value. Also called "exact matching."
        - key   (optional(string), ""): Tag name, must start with one of the following prefixes: `[aws:ResourceTag/]`.
        - value (optional(string), ""): Value of the Tag. Maximum length of 256.
      - string_not_equals  (optional(list(object))): List of conditions for resource tags. Filters the values of your tagged resources for only those resources that you tagged that do not have the same value. Also called "negated matching."
        - key   (optional(string), ""): Tag name, must start with one of the following prefixes: `[aws:ResourceTag/]`.
        - value (optional(string), ""): Value of the Tag. Maximum length of 256.
   - protected_resource_type       (optional(string), ""):       Type of the protected resource.
   - protected_resource_arns       (optional(list(string)), []): ARNs for the protected resources.
   - restore_metadata_overrides    (optional(map(string)):       Override certain restore metadata keys.
DESCRIPTION
  type = map(object({
    name                   = optional(string, "")
    testing_plan_reference = optional(string, "")
    iam_role_arn           = optional(string, "")
    protected_resource_conditions = optional(list(object({
      string_equals = optional(list(object({
        key   = string
        value = string
      })))
      string_not_equals = optional(list(object({
        key   = string
        value = string
      })))
    })), [])
    protected_resource_type    = optional(string, "")
    protected_resource_arns    = optional(list(string), [])
    restore_metadata_overrides = optional(map(string), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = alltrue([for selection in var.restore_testing_selections :
      can(regex("^[a-zA-Z0-9\\_\\.]{2,64}$", selection.name))
    ])
    error_message = "One or more “var.restore_testing_selections.name” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.restore_testing_selections : (
      selection.iam_role_arn == "" || try(provider::aws::arn_parse(selection.iam_role_arn).service, "no") == "iam"
    )])
    error_message = "One or more “var.restore_testing_selections.iam_role_arn” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.restore_testing_selections : (
      contains(["EFS", "FSX", "S3", "NEPTUNE", "AURORA", "DOCUMENTDB", "RDS", "EC2", "EBS", "DYNAMODB"], selection.protected_resource_type)
    )])
    error_message = "One or more “var.restore_testing_selections.protected_resource_type” are invalid."
  }
  validation {
    condition = alltrue([for selection in var.restore_testing_selections : (
      contains(keys(var.restore_testing_plans), selection.testing_plan_reference)
    )])
    error_message = "One or more “var.restore_testing_plans.testing_plan_reference” does not exist."
  }
  validation {
    condition = alltrue([for selection in var.restore_testing_selections : (
      alltrue([for arn in selection.protected_resource_arns : (
        try(provider::aws::arn_parse(arn).service, "no") != "no" || arn == "*"
      )])
    )])
    error_message = "One or more “var.restore_testing_selections.protected_resource_arns” are invalid."
  }
}

variable "restore_selection_iam_role" {
  description = <<-DESCRIPTION
Role to be used for all `var.selections` missing a `iam_role_arn`.
   - name                  required(string):           Name of the IAM Role.
   - description           optional(string, ""):       Description of the IAM Role.
   - path                  optional(string, "/"):      Path of the IAM Role.
   - tags                  optional(map(string), {}):  Tags to assign to the role. Will be merge with `var.tags`.
   - policy_arns           optional(map(string)):      Policies granting the role access to backup and restore. Defaults to IAM service-roles policies provided by AWS. Keys are free values.
DESCRIPTION
  type = object({
    name        = optional(string, "")
    description = optional(string, "")
    path        = optional(string, "/")
    tags        = optional(map(string), {})
    policy_arns = optional(map(string), { 0 = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores" })
  })
  default = null

  validation {
    condition     = var.restore_selection_iam_role == null || can(regex("^[a-zA-Z0-9+=,\\./@-]+$", try(var.restore_selection_iam_role.name, "")))
    error_message = "“var.restore_selection_iam_role.name” is invalid."
  }
  validation {
    condition     = !local.restore_selection_requires_role_creation || var.restore_selection_iam_role != null
    error_message = "“var.restore_selection_iam_role.name” is mandatory since one or more “var.selections” lacks a “iam_role_arn”."
  }
  validation {
    condition = alltrue([for policy in try(var.restore_selection_iam_role.policy_arns, {}) :
      try(provider::aws::arn_parse(policy).service, "no") == "iam" && startswith(try(provider::aws::arn_parse(policy).resource, "no"), "policy")
    ])
    error_message = "One or more “var.restore_selection_iam_role.policy_arns” are invalid."
  }
}

####
# Resources
####

locals {
  restore_selection_requires_role_creation = !alltrue([for selection in var.restore_testing_selections : selection.iam_role_arn != ""])
}

resource "aws_backup_restore_testing_plan" "this" {
  for_each = var.restore_testing_plans

  name                         = "${var.testing_prefix}${each.value.name}"
  schedule_expression          = each.value.schedule_expression
  schedule_expression_timezone = each.value.schedule_expression_timezone
  start_window_hours           = each.value.start_window_hours
  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${each.value.name}"
    }
  )

  recovery_point_selection {
    algorithm             = each.value.recovery_point_selection.algorithm
    include_vaults        = [local.vault_current.arn]
    recovery_point_types  = each.value.recovery_point_selection.recovery_point_types
    selection_window_days = each.value.recovery_point_selection.selection_window_days
  }
}

resource "aws_backup_restore_testing_plan" "replica" {
  for_each = var.replica_enabled ? var.restore_testing_plans : {}

  name                         = "${var.testing_prefix}${each.value.name}"
  schedule_expression          = each.value.schedule_expression
  schedule_expression_timezone = each.value.schedule_expression_timezone
  start_window_hours           = each.value.start_window_hours
  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${each.value.name}"
    }
  )

  recovery_point_selection {
    algorithm             = each.value.recovery_point_selection.algorithm
    include_vaults        = [aws_backup_vault.replica["0"].arn]
    recovery_point_types  = each.value.recovery_point_selection.recovery_point_types
    selection_window_days = each.value.recovery_point_selection.selection_window_days
  }

  provider = aws.replica
}

resource "aws_backup_restore_testing_selection" "this" {
  for_each = var.restore_testing_selections

  name                       = "${var.testing_prefix}${each.value.name}"
  iam_role_arn               = each.value.iam_role_arn != "" ? each.value.iam_role_arn : aws_iam_role.restore_selection["0"].arn
  restore_testing_plan_name  = aws_backup_restore_testing_plan.this[each.value.testing_plan_reference].name
  protected_resource_type    = each.value.protected_resource_type
  protected_resource_arns    = length(each.value.protected_resource_arns) > 0 ? each.value.protected_resource_arns : null
  restore_metadata_overrides = each.value.restore_metadata_overrides

  dynamic "protected_resource_conditions" {
    for_each = each.value.protected_resource_conditions

    content {
      dynamic "string_equals" {
        for_each = protected_resource_conditions.value.string_equals != null ? protected_resource_conditions.value.string_equals : []

        content {
          key   = string_equals.value.key
          value = string_equals.value.value
        }
      }
      dynamic "string_not_equals" {
        for_each = protected_resource_conditions.value.string_not_equals != null ? protected_resource_conditions.value.string_not_equals : []

        content {
          key   = string_not_equals.value.key
          value = string_not_equals.value.value
        }
      }
    }
  }
}

resource "aws_backup_restore_testing_selection" "replica" {
  for_each = var.restore_testing_selections

  name                       = "${var.testing_prefix}${each.value.name}"
  iam_role_arn               = each.value.iam_role_arn != "" ? each.value.iam_role_arn : aws_iam_role.restore_selection["0"].arn
  restore_testing_plan_name  = aws_backup_restore_testing_plan.replica[each.value.testing_plan_reference].name
  protected_resource_type    = each.value.protected_resource_type
  protected_resource_arns    = length(each.value.protected_resource_arns) > 0 ? each.value.protected_resource_arns : null
  restore_metadata_overrides = each.value.restore_metadata_overrides

  dynamic "protected_resource_conditions" {
    for_each = each.value.protected_resource_conditions

    content {
      dynamic "string_equals" {
        for_each = protected_resource_conditions.value.string_equals != null ? protected_resource_conditions.value.string_equals : []

        content {
          key   = string_equals.value.key
          value = string_equals.value.value
        }
      }
      dynamic "string_not_equals" {
        for_each = protected_resource_conditions.value.string_not_equals != null ? protected_resource_conditions.value.string_not_equals : []

        content {
          key   = string_not_equals.value.key
          value = string_not_equals.value.value
        }
      }
    }
  }

  provider = aws.replica
}

resource "aws_iam_role" "restore_selection" {
  for_each = var.restore_selection_iam_role != null ? { 0 = 0 } : {}

  name               = "${var.testing_prefix}${var.restore_selection_iam_role.name}"
  description        = var.restore_selection_iam_role.description
  path               = var.restore_selection_iam_role.path
  assume_role_policy = data.aws_iam_policy_document.selection_assume_role["0"].json

  tags = merge(
    local.tags,
    {
      Name        = "${var.testing_prefix}${var.restore_selection_iam_role.name}"
      Description = var.restore_selection_iam_role.description
    },
    var.restore_selection_iam_role.tags,
  )
}

resource "aws_iam_role_policy_attachment" "restore_selection" {
  for_each = var.restore_selection_iam_role != null ? var.restore_selection_iam_role.policy_arns : {}

  policy_arn = each.value
  role       = aws_iam_role.restore_selection["0"].name
}

resource "aws_iam_role_policy_attachment" "restore_selection_additional" {
  for_each = local.should_create_selection_additional_iam_policy && var.restore_selection_iam_role != null ? { 0 = 0 } : {}

  policy_arn = aws_iam_policy.selection_additional_iam_policy["0"].arn
  role       = aws_iam_role.restore_selection["0"].name
}

####
# Outputs
####

output "aws_backup_restore_testing_plan" {
  value = { for k, v in var.restore_testing_plans :
    k => { for i, j in aws_backup_restore_testing_plan.this[k] : i => j if !contains(["tags"], i) }
  }
}

output "replica_aws_backup_restore_testing_plan" {
  value = var.replica_enabled ? { for k, v in var.restore_testing_plans :
    k => { for i, j in aws_backup_restore_testing_plan.replica[k] : i => j if !contains(["tags"], i) }
  } : null
}

output "restore_selection_aws_iam_role" {
  value = var.restore_selection_iam_role != null ? { for k, v in aws_iam_role.restore_selection["0"] :
    k => v if !contains(["assume_role_policy", "name_prefix", "tags", "inline_policy", "managed_policy_arns", "permissions_boundary"], k)
  } : null
}
