#####
# Minimal
#####

data "aws_backup_vault" "minimal" {
  name = "${local.prefix}${local.minimal_name}"

  depends_on = [
    module.minimal
  ]
}

check "minimal" {
  assert {
    condition = (
      module.minimal.aws_backup_vault.arn == data.aws_backup_vault.minimal.arn &&
      module.minimal.aws_backup_vault.tags_all.test == "minimal" &&
      module.minimal.aws_backup_vault.tags_all.origin == "gitlab.com/wild-beavers/terraform/module-aws-backup" &&
      module.minimal.aws_backup_vault.tags_all.Name == data.aws_backup_vault.minimal.name &&
      module.minimal.replica_aws_backup_vault == null &&
      module.minimal.aws_kms_key == null
    )
    error_message = "Check fail: vault outputs"
  }
}

#####
# Default
#####

data "aws_backup_vault" "default" {
  name = "${local.prefix}${local.default_name}"

  depends_on = [
    module.default
  ]
}

check "default" {
  assert {
    condition = (
      module.default.aws_backup_vault.arn == data.aws_backup_vault.default.arn &&
      module.default.aws_backup_vault.tags_all.test == "default" &&
      module.default.aws_backup_vault.tags_all.origin == "gitlab.com/wild-beavers/terraform/module-aws-backup" &&
      module.default.aws_backup_vault.tags_all.Name == data.aws_backup_vault.default.name &&
      module.default.aws_backup_vault.kms_key_arn == module.default.aws_kms_key.arn &&
      module.default.aws_kms_key.arn == module.default.aws_kms_alias.target_key_arn
    )
    error_message = "Check fail: vault outputs"
  }

  assert {
    condition = (
      module.default.replica_aws_backup_vault.arn != data.aws_backup_vault.default.arn &&
      module.default.replica_aws_backup_vault.tags_all.test == "default" &&
      module.default.replica_aws_backup_vault.tags_all.origin == "gitlab.com/wild-beavers/terraform/module-aws-backup" &&
      module.default.replica_aws_backup_vault.tags_all.Name == data.aws_backup_vault.default.name &&
      module.default.replica_aws_backup_vault.kms_key_arn == module.default.replica_aws_kms_key.arn &&
      module.default.replica_aws_kms_key.arn == module.default.replica_aws_kms_alias.target_key_arn
    )
    error_message = "Check fail: vault replica outputs"
  }

  assert {
    condition = (
      module.default.aws_backup_plans["0"].name == module.default.aws_backup_plans["0"].tags_all.Name &&
      tolist(module.default.aws_backup_plans["0"].rule)[0].start_window == 60 &&
      tolist(module.default.aws_backup_plans["0"].rule)[0].completion_window == 120 &&
      length(tolist(module.default.aws_backup_plans["0"].rule)[0].lifecycle) == 0 &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[0].copy_action)[0].destination_vault_arn == module.default.replica_aws_backup_vault.arn &&
      tolist(module.default.aws_backup_plans["0"].rule)[1].lifecycle[0].cold_storage_after == 30 &&
      tolist(module.default.aws_backup_plans["0"].rule)[1].lifecycle[0].delete_after == 365 &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[0].destination_vault_arn == module.minimal.aws_backup_vault.arn &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[0].lifecycle[0].cold_storage_after == 30 &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[0].lifecycle[0].delete_after == 365 &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[1].destination_vault_arn == module.default.replica_aws_backup_vault.arn &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[1].lifecycle[0].cold_storage_after == 30 &&
      tolist(tolist(module.default.aws_backup_plans["0"].rule)[1].copy_action)[1].lifecycle[0].delete_after == 365 &&
      length(module.default.aws_backup_plans["0"].rule) == 2
    )
    error_message = "Check fail: plans outputs"
  }

  assert {
    condition     = module.default.aws_iam_policies == null
    error_message = "Check fail: identity-based policies outputs"
  }

  assert {
    condition = (
      module.default.precomputed.aws_backup_vault_arn == module.default.aws_backup_vault.arn &&
      module.default.precomputed.aws_backup_vault_replica_arn == module.default.replica_aws_backup_vault.arn &&
      contains(module.default.precomputed.vault_kms_key.aws_kms_aliases[0].arns, module.default.aws_kms_alias.arn) &&
      contains(module.default.precomputed.vault_kms_key.aws_kms_aliases[0].arns, module.default.replica_aws_kms_alias.arn)
    )
    error_message = "Check fail: precomputed outputs"
  }
}

#####
# Policies
#####

data "aws_backup_vault" "policies" {
  name = "${local.prefix}${local.policies_name}"

  depends_on = [
    module.policies
  ]
}

check "policies" {
  assert {
    condition = (
      module.policies.aws_backup_vault.arn == data.aws_backup_vault.policies.arn &&
      module.policies.aws_backup_vault.tags_all.test == "policies" &&
      module.policies.aws_backup_vault.tags_all.origin == "gitlab.com/wild-beavers/terraform/module-aws-backup" &&
      module.policies.aws_backup_vault.tags_all.Name == data.aws_backup_vault.policies.name &&
      module.policies.aws_backup_vault.kms_key_arn == module.policies.aws_kms_key.arn &&
      module.policies.aws_kms_key.arn == module.policies.aws_kms_alias.target_key_arn
    )
    error_message = "Check fail: vault outputs"
  }

  assert {
    condition = (
      module.policies.replica_aws_backup_vault.arn != data.aws_backup_vault.policies.arn &&
      module.policies.replica_aws_backup_vault.tags_all.test == "policies" &&
      module.policies.replica_aws_backup_vault.tags_all.origin == "gitlab.com/wild-beavers/terraform/module-aws-backup" &&
      module.policies.replica_aws_backup_vault.tags_all.Name == data.aws_backup_vault.policies.name &&
      module.policies.replica_aws_backup_vault.kms_key_arn == module.policies.replica_aws_kms_key.arn &&
      module.policies.replica_aws_kms_key.arn == module.policies.replica_aws_kms_alias.target_key_arn
    )
    error_message = "Check fail: vault replica outputs"
  }

  assert {
    condition = (
      jsondecode(module.policies.aws_iam_policies.full.json).Statement[0].Action == "kms:*" &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json).Statement[0].Resource, module.policies.aws_kms_alias.arn) &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json).Statement[0].Resource, module.policies.replica_aws_kms_alias.arn) &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json).Statement[1].Resource, module.policies.aws_backup_vault.arn) &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json).Statement[1].Resource, module.policies.replica_aws_backup_vault.arn)
    )
    error_message = "Check fail: identity-based full policy"
  }

  assert {
    condition = (
      module.policies.selection_additional_aws_iam_policy != null
    )
    error_message = "Check fail: additional policy"
  }

  assert {
    condition = (
      module.policies.precomputed.backup_selection_aws_iam_role_arn == module.policies.backup_selection_aws_iam_role.arn &&
      module.policies.precomputed.aws_backup_vault_arn == module.policies.aws_backup_vault.arn &&
      module.policies.precomputed.aws_backup_vault_replica_arn == module.policies.replica_aws_backup_vault.arn &&
      contains(module.policies.precomputed.vault_kms_key.aws_kms_aliases[0].arns, module.policies.aws_kms_alias.arn) &&
      contains(module.policies.precomputed.vault_kms_key.aws_kms_aliases[0].arns, module.policies.replica_aws_kms_alias.arn) &&
      module.policies.aws_iam_policies.full.arn == module.policies.precomputed.aws_iam_policies.full.arn &&
      module.policies.aws_iam_policies.full.name == module.policies.precomputed.aws_iam_policies.full.name &&
      module.policies.aws_iam_policies.ro.arn == module.policies.precomputed.aws_iam_policies.ro.arn &&
      module.policies.aws_iam_policies.ro.name == module.policies.precomputed.aws_iam_policies.ro.name
    )
    error_message = "Check fail: precomputed outputs"
  }
}

#####
# Notification
#####

check "notification" {
  assert {
    condition = (
      lookup(module.notification.precomputed, "aws_backup_vault_replica_arn", null) == null &&
      module.policies.precomputed.aws_backup_vault_arn == module.policies.aws_backup_vault.arn
    )
    error_message = "Check fail: vault outputs"
  }
}
