data "aws_caller_identity" "current" {}

data "aws_region" "current" {}
data "aws_region" "replica" {
  provider = aws.replica
}

data "aws_partition" "current" {}

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "tftest"
  }
}

####
# Default
####

resource "aws_iam_role" "default" {
  name = "${local.prefix}${local.default_name}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "backup.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "backup" {
  policy_arn = "arn:${data.aws_partition.current.partition}:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.default.name
}

resource "aws_iam_role_policy_attachment" "restore" {
  policy_arn = "arn:${data.aws_partition.current.partition}:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"
  role       = aws_iam_role.default.name
}

locals {
  default_name = "tftestdefault"
}

module "default" {
  source = "../../"

  testing_prefix     = local.prefix
  current_account_id = data.aws_caller_identity.current.account_id
  current_partition  = data.aws_partition.current.partition
  current_region     = data.aws_region.current.name
  replica_enabled    = true

  vault_kms_key = {
    alias = local.default_name
  }
  vault = {
    name          = local.default_name
    force_destroy = true
    tags = {
      test = "default"
    }
  }
  plans = {
    0 = {
      name = local.default_name
      rules = {
        rule1 = {
          name              = local.default_name
          schedule          = "cron(0 12 * * ? *)"
          start_window      = 60
          completion_window = 120
          recovery_point_tags = {
            Name = "tftest-plan-rule1"
          }
        }
        rule2 = {
          name     = "${local.default_name}2"
          schedule = "cron(0 7 ? * * *)"
          lifecycle = {
            cold_storage_after = 30
            delete_after       = 365
          }
          recovery_point_tags = {
            Name = "tftest-plan-rule2"
          }
          copy_action = {
            destination_vault_arn = module.minimal.aws_backup_vault.arn
          }
        }
      }
    }

    1 = {
      name = "${local.default_name}2"
      rules = {
        0 = {
          name     = local.default_name
          schedule = "cron(0 7 ? * * *)"
        }
      }
    }
  }

  selections = {
    0 = {
      name           = "resources"
      plan_reference = "0"
      iam_role_arn   = aws_iam_role.default.arn
      resources      = [aws_ebs_volume.example.arn]
    }
    1 = {
      name           = "tags"
      plan_reference = "1"
      iam_role_arn   = aws_iam_role.default.arn
      selection_tags = [
        {
          type  = "STRINGEQUALS"
          key   = "Backup"
          value = "true"
        },
        {
          type  = "STRINGEQUALS"
          key   = "Backup"
          value = "1"
        },
      ]
    }
  }

  restore_testing_plans = {
    0 = {
      name                = local.default_name
      schedule_expression = "cron(0 12 ? * * *)"
      start_window_hours  = 4
      recovery_point_selection = {
        algorithm            = "LATEST_WITHIN_WINDOW"
        recovery_point_types = ["CONTINUOUS", "SNAPSHOT"]
      }
    }
  }
  restore_testing_selections = {
    test = {
      name                    = "selection"
      testing_plan_reference  = "0"
      protected_resource_type = "EBS"
      iam_role_arn            = aws_iam_role.default.arn
      protected_resource_arns = [aws_ebs_volume.example.arn]
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

####
# Minimal
####

locals {
  minimal_name = "tftestminimal"
}

module "minimal" {
  source = "../../"

  testing_prefix = local.prefix

  vault = {
    name          = local.minimal_name
    force_destroy = true
    tags = {
      test = "minimal"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

####
# Policies
####

locals {
  policies_name             = "tftestpolicies"
  policies_iam_role_example = "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:role/policies-example"
}

resource "aws_iam_policy" "policies" {
  name = "${local.prefix}${local.default_name}"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action      = "*"
        Effect      = "Deny"
        NotResource = [aws_ebs_volume.example.arn]
        Condition = {
          StringEquals = {
            "aws:ResourceTag/something_more" = ["true"]
          }
        }
      },
    ]
  })
}

module "policies" {
  source = "../../"

  testing_prefix     = local.prefix
  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  current_partition  = data.aws_partition.current.partition
  replica_region     = data.aws_region.replica.name
  replica_enabled    = true

  vault_kms_key = {
    alias = local.policies_name
  }
  vault = {
    name          = local.policies_name
    force_destroy = true
    tags = {
      test = "policies"
    }
  }
  vault_iam_policy_identity = {
    name_template        = "${local.policies_name}%s"
    description_template = "Some test ${local.policies_name} %s"
    path                 = "/test/"
  }
  vault_iam_policy_identity_export_jsons   = true
  vault_iam_policy_sid_prefix              = "poltest"
  vault_iam_policy_export_actions          = true
  vault_iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]
  vault_iam_policy_entity_arns = {
    full = {
      caller = local.caller_arn
    }
    ro = {
      example = local.policies_iam_role_example
    }
  }

  plans = {
    plan = {
      name = local.policies_name
      rules = {
        rule1 = {
          name              = local.policies_name
          schedule          = "cron(0 12 * * ? *)"
          start_window      = 120
          completion_window = 180
          lifecycle = {
            cold_storage_after = 10
            delete_after       = 120
          }
        }
      }
    }
  }
  selections = {
    main = {
      name           = "tags"
      plan_reference = "plan"
      selection_tags = [
        {
          type  = "STRINGEQUALS"
          key   = "NeedsBackup"
          value = "yes"
        },
      ]
      resources = [aws_ebs_volume.example.arn]
    }
  }
  selection_iam_role = {
    name = local.policies_name
    policy_arns = {
      0 = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
      1 = aws_iam_policy.policies.arn
    }
    description = "The service role to make backups"
    path        = "/tftestbackup/"
  }
  selection_additional_iam_policy = {
    name        = local.policies_name
    description = "Restrict backups and restores"
    path        = "/tftestbackup/"
    restricted_tags = {
      protected = "true"
      backup    = "yes"
    }
  }

  restore_testing_plans = {
    restore = {
      name                = local.policies_name
      schedule_expression = "cron(0 8 ? * * *)"
      start_window_hours  = 8
      recovery_point_selection = {
        algorithm            = "LATEST_WITHIN_WINDOW"
        recovery_point_types = ["SNAPSHOT"]
      }
    }
  }
  restore_selection_iam_role = {
    name = "${local.policies_name}restore"
    policy_arns = {
      0 = "arn:${data.aws_partition.current.partition}:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"
      1 = aws_iam_policy.policies.arn
    }
    description = "The service role to make restores"
    path        = "/tftestrestores/"
  }
  restore_testing_selections = {
    ebs = {
      name                    = "selection_by_tag_ebs"
      testing_plan_reference  = "restore"
      protected_resource_type = "EBS"
      protected_resource_conditions = [
        {
          string_equals = [{
            key   = "aws:ResourceTag/backup"
            value = "true"
          }]
        }
      ]
    }
    rds = {
      name                    = "selection_by_tag_rds"
      testing_plan_reference  = "restore"
      protected_resource_type = "RDS"
      protected_resource_conditions = [
        {
          string_equals = [{
            key   = "aws:ResourceTag/backup"
            value = "true"
          }]
        }
      ]
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

####
# Notifications
####

resource "aws_sns_topic" "notification" {
  name = "${local.prefix}${local.notif_name}"
}

locals {
  notif_name = "tftestnotif"
}

module "notification" {
  source = "../../"

  testing_prefix     = local.prefix
  current_account_id = data.aws_caller_identity.current.account_id
  current_partition  = data.aws_partition.current.partition
  current_region     = data.aws_region.current.name

  vault_kms_key = {
    alias = local.notif_name
  }
  vault = {
    logically_air_gapped    = true
    lock_max_retention_days = 60
    lock_min_retention_days = 7
    name                    = local.notif_name
    force_destroy           = true
    sns_topic_arn           = aws_sns_topic.notification.arn
    backup_vault_events     = ["BACKUP_JOB_COMPLETED", "RESTORE_JOB_COMPLETED"]
    tags = {
      test = "notif"
    }
  }
  vault_iam_policy_identity = {
    name_template        = "${local.notif_name}%s"
    description_template = "Some test ${local.notif_name} %s"
    path                 = "/test/"
  }
  vault_iam_policy_entity_arns = {
    full = {
      caller = local.caller_arn
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

####
# Global
####

# Commented, so it does not disrupt the entire testing account

# module "global" {
#   source = "../../"
#
#   global_settings_cross_account_enabled = true
#
#   providers = {
#     aws.replica = aws.replica
#   }
# }
