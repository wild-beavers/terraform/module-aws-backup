####
# Minimal
####

output "minimal" {
  value = module.minimal
}

####
# Default
####

output "default" {
  value = module.default
}

####
# Policies
####

output "policies" {
  value = module.policies
}

####
# Notification
####

output "notification" {
  value = module.notification
}
