terraform {
  required_version = ">= 1.9"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 3.73"
      configuration_aliases = [aws.replica]
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3"
    }
  }
}
