####
# Variables
####

variable "global_settings_cross_account_enabled" {
  description = "Whether or not to allow cross account backups. This only works from the root account."
  type        = bool
  default     = false
  nullable    = false
}

variable "region_settings" {
  description = <<-DOCUMENTATION
Manage region-level settings for AWS Backup.
  - resource_type_opt_in_preference     (required, map(string)): Service names along with the opt-in preferences for the Region.
  - resource_type_management_preference (optional, map(string)): Service names along with the management preferences for the Region. For more information, see the [AWS Documentation](https://docs.aws.amazon.com/aws-backup/latest/devguide/API_UpdateRegionSettings.html#API_UpdateRegionSettings_RequestSyntax).
DOCUMENTATION
  type = object({
    resource_type_opt_in_preference     = map(string)
    resource_type_management_preference = optional(map(string), null)
  })
  default = null
}

####
# Resources
####

resource "aws_backup_global_settings" "this" {
  for_each = var.global_settings_cross_account_enabled ? { 0 = 0 } : {}

  global_settings = {
    isCrossAccountBackupEnabled = var.global_settings_cross_account_enabled
  }
}

resource "aws_backup_region_settings" "this" {
  for_each = var.region_settings != null ? { 0 = 0 } : {}

  resource_type_opt_in_preference     = var.region_settings.resource_type_opt_in_preference
  resource_type_management_preference = var.region_settings.resource_type_management_preference
}
