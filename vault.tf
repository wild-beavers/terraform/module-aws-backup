####
# Variables
####

variable "vault" {
  description = <<-DESCRIPTION
    logically_air_gapped (optional(bool, false)): Whether to make a logically air-gapped Backup Vault

    name                (required(map(string))):     Name of the Backup Vault to create. Must contain the "name" key.
    force_destroy       (optional(bool, false)):     Indicates that all recovery points stored in the Vault are deleted so that the Vault can be destroyed without error.
    kms_key_arn         (optional(string, "")):      ARN of the server-side encryption key that is used to protect your backups. Conflicts with `var.vault_kms_key`.
    kms_key_replica_arn (optional(string, "")):      ARN of the replica of the the server-side encryption key that is used to protect your backups. Conflicts with `var.vault_kms_key`. Ignored if `var.replica_enabled` is `false`.
    tags                (optional(map(string), {})): Tags to assign specifically to the Backup Vault. Will be merged with `var.tags`.

    lock_changeable_for_days (optional(number)): Number of days before the lock date, a grace period. If omitted creates a vault lock in `governance` mode - no real lock, otherwise it will create a vault lock in `compliance` mode, locked from any kind of deletion.
    lock_max_retention_days  (optional(number)): Maximum retention period that the Vault retains its recovery points.
    lock_min_retention_days  (optional(number)): Minimum retention period that the vault retains its recovery points.

    sns_topic_arn       (optional(string, "")):       ARN that specifies the topic for a Backup Vault’s events
    backup_vault_events (optional(list(string), [])): Events that indicate the status of jobs to back up resources to the backup vault.

    policy (optional(string, ""): Backup vault access policy document in JSON format. Should `var.vault_iam_policy_xxx` be also provided, this would be used as an additional policy merged with the generated resource-based policy.
DESCRIPTION
  type = object({
    logically_air_gapped = optional(bool, false)

    name                = optional(string, "")
    force_destroy       = optional(bool, false)
    kms_key_arn         = optional(string, "")
    kms_key_replica_arn = optional(string, "")
    tags                = optional(map(string), {})

    lock_changeable_for_days = optional(number)
    lock_max_retention_days  = optional(number)
    lock_min_retention_days  = optional(number)

    sns_topic_arn       = optional(string, "")
    backup_vault_events = optional(list(string), [])

    policy = optional(string, "")
  })
  default = null

  validation {
    condition     = var.vault == null || can(regex("^[a-zA-Z0-9\\-\\_]{2,50}$", try(var.vault.name, "")))
    error_message = "“var.vault.name” must match “^[a-zA-Z0-9\\-\\_]{2,50}$”."
  }
  validation {
    condition     = try(var.vault.policy, "") == "" || can(jsonencode(try(var.vault.policy, "")))
    error_message = "“var.vault.vault_policy” must be a valid JSON document."
  }
  validation {
    condition     = try(var.vault.lock_changeable_for_days, null) == null || (coalesce(try(var.vault.lock_changeable_for_days, null), 3) >= 3 && coalesce(try(var.vault.lock_changeable_for_days, null), 3) <= 36500)
    error_message = "“var.vault.lock_changeable_for_days” must be between 3 and 36500."
  }
  validation {
    condition     = try(var.vault.lock_max_retention_days, null) == null || (coalesce(try(var.vault.lock_max_retention_days, null), 1) >= 1 && coalesce(try(var.vault.lock_max_retention_days, null), 1) <= 36500)
    error_message = "“var.vault.lock_max_retention_days” must be between 1 and 36500."
  }
  validation {
    condition     = try(var.vault.lock_min_retention_days, null) == null || (coalesce(try(var.vault.lock_min_retention_days, null), 1) >= 1 && coalesce(try(var.vault.lock_min_retention_days, null), 1) <= 36500)
    error_message = "“var.vault.lock_min_retention_days”  must be between 1 and 36500."
  }
  validation {
    condition     = try(var.vault.kms_key_arn, "") == "" || try(provider::aws::arn_parse(try(var.vault.kms_key_arn, "")).service, "no") == "kms"
    error_message = "“var.vault.kms_key_arn” is invalid."
  }
  validation {
    condition     = try(var.vault.kms_key_replica_arn, "") == "" || try(provider::aws::arn_parse(try(var.vault.kms_key_replica_arn, "")).service, "no") == "kms"
    error_message = "“var.vault.kms_key_replica_arn” is invalid."
  }
  validation {
    condition     = try(var.vault.sns_topic_arn, "") == "" || try(provider::aws::arn_parse(try(var.vault.sns_topic_arn, "")).service, "no") == "sns"
    error_message = "“var.vault.sns_topic_arn” is invalid."
  }
  validation {
    condition     = try(var.vault.sns_topic_arn, "") == "" || length(try(var.vault.backup_vault_events, {})) > 0
    error_message = "“var.vault.backup_vault_events” are required when “var.vault.sns_topic_arn” is given."
  }
  validation {
    condition     = alltrue([for event in try(var.vault.backup_vault_events, {}) : contains(["BACKUP_JOB_STARTED", "BACKUP_JOB_COMPLETED", "COPY_JOB_STARTED", "COPY_JOB_SUCCESSFUL", "COPY_JOB_FAILED", "RESTORE_JOB_STARTED", "RESTORE_JOB_COMPLETED", "RECOVERY_POINT_MODIFIED"], event)])
    error_message = "One or more “var.vault.backup_vault_events” are invalid."
  }
  validation {
    condition     = !try(var.vault.logically_air_gapped, false) || (try(var.vault.lock_max_retention_days, null) != null && try(var.vault.lock_min_retention_days, null) != null)
    error_message = "“var.vault.logically_air_gapped” is set, so “var.vault.lock_max_retention_days” and “var.vault.lock_min_retention_days” is mandatory."
  }
}

####
# Resources Vault
####

locals {
  vault_should_create_lock          = lookup(var.vault, "lock_changeable_for_days", null) != null || lookup(var.vault, "lock_min_retention_days", null) != null || lookup(var.vault, "lock_max_retention_days", null) != null
  vault_should_create_notifications = length(var.vault.backup_vault_events) > 0
  vault_current                     = !var.vault.logically_air_gapped ? aws_backup_vault.this["0"] : aws_backup_logically_air_gapped_vault.this["0"]
}

resource "aws_backup_vault" "this" {
  for_each = var.vault != null && !var.vault.logically_air_gapped ? { 0 = 0 } : {}

  force_destroy = var.vault.force_destroy
  name          = "${var.testing_prefix}${var.vault.name}"
  kms_key_arn   = local.vault_kms_key_id

  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${var.vault.name}"
    },
    var.vault.tags,
  )
}

resource "aws_backup_vault_lock_configuration" "this" {
  for_each = local.vault_should_create_lock && !var.vault.logically_air_gapped ? { 0 = 0 } : {}

  backup_vault_name   = aws_backup_vault.this["0"].name
  changeable_for_days = var.vault.lock_changeable_for_days
  max_retention_days  = var.vault.lock_max_retention_days
  min_retention_days  = var.vault.lock_min_retention_days
}

resource "aws_backup_vault_policy" "this" {
  for_each = var.vault != null && local.vault_should_have_resource_based_policy ? { 0 = 0 } : {}

  backup_vault_name = local.vault_current.name
  policy            = module.iam_policy["0"].resource_based_aws_iam_policy_document.json
}

resource "aws_backup_vault_notifications" "this" {
  for_each = local.vault_should_create_notifications ? { 0 = 0 } : {}

  backup_vault_name   = local.vault_current.name
  sns_topic_arn       = var.vault.sns_topic_arn
  backup_vault_events = var.vault.backup_vault_events
}

resource "aws_backup_vault" "replica" {
  for_each = var.vault != null && !var.vault.logically_air_gapped && var.replica_enabled ? { 0 = 0 } : {}

  force_destroy = var.vault.force_destroy
  name          = "${var.testing_prefix}${var.vault.name}"
  kms_key_arn   = local.vault_kms_replica_key_id

  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${var.vault.name}"
    },
    var.vault.tags,
  )

  provider = aws.replica
}

resource "aws_backup_vault_lock_configuration" "replica" {
  for_each = local.vault_should_create_lock && var.replica_enabled && !var.vault.logically_air_gapped ? { 0 = 0 } : {}

  backup_vault_name   = aws_backup_vault.replica["0"].name
  changeable_for_days = var.vault.lock_changeable_for_days
  max_retention_days  = var.vault.lock_max_retention_days
  min_retention_days  = var.vault.lock_min_retention_days

  provider = aws.replica
}

resource "aws_backup_vault_policy" "replica" {
  for_each = var.vault != null && var.replica_enabled && !var.vault.logically_air_gapped && local.vault_should_have_resource_based_policy ? { 0 = 0 } : {}

  backup_vault_name = aws_backup_vault.replica["0"].name
  policy            = module.iam_policy["0"].resource_based_aws_iam_policy_document.json

  provider = aws.replica
}

resource "aws_backup_logically_air_gapped_vault" "this" {
  for_each = var.vault != null && var.vault.logically_air_gapped && local.vault_should_have_resource_based_policy ? { 0 = 0 } : {}

  name               = "${var.testing_prefix}${var.vault.name}"
  max_retention_days = var.vault.lock_max_retention_days
  min_retention_days = var.vault.lock_min_retention_days

  tags = merge(
    local.tags,
    {
      Name = "${var.testing_prefix}${var.vault.name}"
    },
    var.vault.tags,
  )
}

####
# Outputs
####

output "aws_backup_vault" {
  value = var.vault != null ? { for k, v in local.vault_current :
    k => v if !contains(["tags", "recovery_points", "timeouts"], k)
  } : null
}

output "replica_aws_backup_vault" {
  value = var.vault != null && var.replica_enabled ? { for k, v in aws_backup_vault.replica["0"] :
    k => v if !contains(["tags", "recovery_points", "timeouts"], k)
  } : null
}
